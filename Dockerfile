FROM node:18-bullseye-slim
RUN apt-get update \
 && apt-get install --install-recommends --yes wget xvfb \
 && wget -qO chrome.deb https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb \
 && apt-get install --no-install-recommends --yes ./chrome.deb \
 && rm chrome.deb \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

CMD [ "bash", "-c" ]
