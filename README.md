# Docker Node.js Chrome

This repository builds a Docker image containing Node.js and Google Chrome.

The Docker image contains Node.js 18 and the latest version of Google Chrome. It’s built daily at
04:00 AM UTC.

## Building

The image can be built locally using `docker build`:

```sh
docker build .
```

## License

[LGPL-3.0-only](LICENSE.md) © [Appsemble](https://appsemble.com)
